from const import now_1


def decor(func):

    def inner_func():
        print("Приветствуем в нашем интернет-магазине!\nМестные дата и время:", now_1)
        a = func()
        print("Спасибо, что выбрали нас!")
        return a
    return inner_func


@decor
def print_menu():
    print("0. Выход")
    print("1. Список товаров")
    print("2. Показать товары не дороже определённой стоимости")


def print_item(li):
    for item in li:
        print(item)
