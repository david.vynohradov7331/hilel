from const import GOODS
from helpers import print_menu, print_item


def start_store():

    while True:

        print_menu()

        option = input("Пожалуйста, выберите одну из опций выше: ")

        if option == "0":
            break
        if option == "1":
            print_item(GOODS)
            continue
        if option == "2":
            max_price = float(input("Пожалуйста, введите максимальную стоимость: "))
            print_item([item for item in GOODS if item.price <= max_price])
        else:
            print("Вы можете выбрать только одну из перечисленных опций по её порядковому номеру в списке. "
                  "Пожалуйста, попробуйте ещё раз.")


if __name__ == '__main__':
    start_store()
