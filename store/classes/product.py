class Merch:

    def __init__(self, article, title, price):
        self.article = article
        self.title = title
        self.price = price

    def __str__(self):
        return f'>>Артикул: {self.article}<< >>Наименование: {self.title}<< >>Цена: {self.price}<<'

    @property
    def get_price(self):
        return self.price
