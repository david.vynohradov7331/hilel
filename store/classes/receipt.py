from row import Row
from store.const import now_1


class Receipt:

    def __init__(self, customer):
        self.date_time = now_1
        self.customer = customer
        self.rows = []

    def add_row(self, r: Row):
        self.rows.append(r)
