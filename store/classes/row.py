from product import Merch


class Row:

    def __init__(self, product: Merch, n):
        self.product = product
        self.n = n

    def __str__(self):
        return f'Товар: {self.product} Количество: {self.n} Общая стоимость: {self.product.price * self.n}'
