from store.classes.product import Merch
from datetime import datetime

GOODS = [Merch("12345", "Картошка вкусная", 15.0),
         Merch("22345", "Картошка невкусная", 14.5),
         Merch("32345", "Лучок ароматный", 5.3),
         Merch("42345", "Лучок ароматнее", 7.3),
         Merch("52345", "Арбузик херсонский", 10.0),
         Merch("62345", "Арбузик с-под Жулян", 8.0),
         Merch("72345", "Базилик зеленый", 35.0),
         Merch("82345", "Базилик черный", 35.0),
         Merch("92345", "Орех грецкий", 150.0),
         Merch("13345", "Орех кешью", 250.0)]

now = datetime.now()
now_1 = now.strftime("%d/%m/%Y %H:%M:%S")
