var_1 = input("Введите текст: ")
var_2 = int(input("Введите целое число: "))


def func(input_string, input_bit):

    if input_bit & 0b1:
        input_string = f"{'<b>'}{input_string}{'<b>'}"
    if input_bit & 0b10:
        input_string = f"{'<i>'}{input_string}{'<i>'}"
    if input_bit & 0b100:
        input_string = f"{'<u>'}{input_string}{'<u>'}"

    return input_string


print(func(var_1, var_2))
