print("Арифметические операции и выведение результатов.")

a = 2*3
print("a=", a)
print("Тип а=", type(a))

b = ((3*3+8)/3)
print("b=", b)
print("Тип b=", type(b))

c = 8//3
print("c=", c)
print("Тип c=", type(c))

d = 8 % 3
print("d=", d)
print("Тип d=", type(d))

e = 5**2
print("e=", e)
print("Тип e=", type(e))

f = 'Hello'+'world'
print("f=", f)
print("Тип f=", type(f))

print("Переменные со строками.")
var_1 = "Это строка текста."
print(var_1)
var_2 = 'Это еще одна строка текста.'
print(var_2)

print("Работа со слайсами.")

print(var_1[:11])

print(var_1[2:12])

print(var_1[-10:])

print(var_1[::-1])

print(var_1[1::2])

print(var_1[::2])

calc_1 = input("Введите первое число: ")
calc_2 = input("Введите второе число: ")

print("Сумма введённых чисел =", int(calc_1)+int(calc_2))
