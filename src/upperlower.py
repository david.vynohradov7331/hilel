# this is for hw5

string_1 = """to the well-organized mind, death is but the next great adventure
ALL WE HAVE TO DECIDE IS WHAT TO DO WITH THE TIME THAT IS GIVEN US
and some things that should not have been forgotten were lost. 
history became legend. legend became myth. and for two and a 
half thousand years, the ring passed out of all knowledge."""

for i in string_1:
    if ord(i) in range(97, 123):
        print(chr(ord(i) - 32), end="")
    elif ord(i) in range(65, 91):
        print(chr(ord(i) + 32), end="")
    else:
        print(i, end="")
